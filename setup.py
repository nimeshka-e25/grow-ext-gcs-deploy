"""Setup script for extension."""

from setuptools import setup

setup(
    name='grow-ext-gcs-deploy',
    version='1.0.1',
    license='MIT',
    author='E25',
    author_email='@',
    include_package_data=False,
    packages=[
        'gcs_deploy',
    ],
    install_requires=[
        'boto3==1.0.0',
    ],
)
