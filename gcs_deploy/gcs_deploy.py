"""GCS Deploy extension"""

from grow import extensions
from grow.extensions import hooks
from grow.documents import document
from . import google_cloud_storage

class GCSDeploymentRegisterHook(hooks.DeploymentRegisterHook):
    """Handle the deploy register hook."""

    def should_trigger(self, previous_result, *_args, **_kwargs):
        """Determine if the hook should trigger."""
        return self.extension.config.get('enabled', True)

    def trigger(self, previous_result, deployments, *_args, **_kwargs):
        """Trigger the deployment destination registration hook."""
        deployments.register_destination(
            google_cloud_storage.GoogleCloudStorageDestination)


class GCSDeployExtension(extensions.BaseExtension):
    """GCS deploy extension"""

    @property
    def available_hooks(self):
        """Returns the available hook classes."""
        return [GCSDeploymentRegisterHook]
